(function () {
    angular
        .module("pageTrans")
        .config(uiRouteConfig);

    uiRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uiRouteConfig($stateProvider, $urlRouterProvider){
        $stateProvider  
            .state('home',{
                url : '/home',
                templateUrl: "app/home/home.html"
            })
            .state("aboutMe", {
                url: "/about",
                templateUrl: "app/about/about.html"
            });

        $urlRouterProvider.otherwise("/home");
    }
})();